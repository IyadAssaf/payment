package mock

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"fmt"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/IyadAssaf/payment/store/shared"
)

// Store - a mocked store
type Store struct {
	ReturnRowsList []sqlmock.Rows
	rowIdx         int
	FailQuery      bool
}

// New - returns a new mock store based on optios
func New(returnRowsList []sqlmock.Rows) *Store {
	return &Store{
		ReturnRowsList: returnRowsList,
		rowIdx:         0,
	}
}

// Connect doesn't need to connect to anything
func (s *Store) Connect() error {
	return nil
}

// QueryContext - query the store
func (s *Store) QueryContext(context.Context, string, ...interface{}) (*sql.Rows, error) {
	if s.FailQuery {
		return nil, fmt.Errorf("Mock failing")
	}
	if s.rowIdx >= len(s.ReturnRowsList) {
		return nil, fmt.Errorf("Not enough mock queries")
	}

	rows, err := convertMockRows(s.ReturnRowsList[s.rowIdx])
	if err != nil {
		return nil, err
	}
	s.rowIdx++
	return rows, nil
}

// Begin - start a transaction using store.Txable interface
func (s *Store) Begin() (shared.Txable, error) {
	return nil, nil
}

// EmptyRow - return no rows
var EmptyRow = MapToRows([]string{}, []map[string]interface{}{})

// taken from https://github.com/DATA-DOG/go-sqlmock/issues/42
func convertMockRows(mockRows sqlmock.Rows) (*sql.Rows, error) {
	db, mock, err := sqlmock.New()
	if err != nil {
		return nil, err
	}

	mock.ExpectQuery("select").WillReturnRows(&mockRows)
	return db.Query("select")
}

// MapToRows - map mock values to to column names in the correct order, return mocked rows
func MapToRows(colNames []string, mockValsRows []map[string]interface{}) sqlmock.Rows {
	rows := sqlmock.NewRows(colNames)
	if mockValsRows == nil {
		return *rows
	}

	for _, mockVals := range mockValsRows {
		if mockVals == nil {
			continue
		}

		columnData := make([]driver.Value, len(colNames))
		for cIdx, cn := range colNames {
			columnData[cIdx] = mockVals[cn]
		}

		fmt.Printf("Got %d columns and %d data\n", len(colNames), len(columnData))
		rows.AddRow(columnData...)
	}

	return *rows
}

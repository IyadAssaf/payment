package store

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/IyadAssaf/payment/store/postgres"
	"gitlab.com/IyadAssaf/payment/store/shared"
)

// Storeable - interface for data storage
type Storeable interface {
	Connect() error
	QueryContext(ctx context.Context, q string, args ...interface{}) (*sql.Rows, error)
	Begin() (shared.Txable, error)
}

// Init - return a storeable object
func Init() (Storeable, error) {
	s := postgres.New()
	if err := s.Connect(); err != nil {
		return nil, err
	}
	return s, nil
}

// ErrTransactionNotStarted - transaction hasn't been started so we cannot commit or rollback
var ErrTransactionNotStarted = fmt.Errorf("Transaction invalid: not started")

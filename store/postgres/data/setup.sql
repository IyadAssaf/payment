CREATE TABLE resources (
    id bigserial PRIMARY KEY,

    path text NOT NULL,
    -- TOOD: set as emnum
    method text NOT NULL,

    created_at timestamp NOT NULL DEFAULT current_timestamp,
    updated_at timestamp
);

CREATE TABLE authorizations (
    id bigserial PRIMARY KEY,

    token text NOT NULL,
    expires_at timestamp,

    created_at timestamp NOT NULL DEFAULT current_timestamp,
    updated_at timestamp
);

CREATE TABLE resource_authorizations (
    id bigserial PRIMARY KEY,

    authorization_id integer REFERENCES authorizations (id) NOT NULL,
    resource_id integer REFERENCES resources (id),

    created_at timestamp NOT NULL DEFAULT current_timestamp,
    updated_at timestamp
);

CREATE TABLE parties (
    id bigserial PRIMARY KEY,

    account_number text NOT NULL,
    bank_id text NOT NULL,
    bank_id_code text NOT NULL,
    -- TODO: set as enum
    account_type text NOT NULL,

    -- nullable based on type
    account_name text,
    name text,
    address text,
    account_number_code text,

    created_at timestamp NOT NULL DEFAULT current_timestamp,
    updated_at timestamp
); 

CREATE TABLE payments (
    id bigserial PRIMARY KEY,
        
    amount text NOT NULL,
    currency text NOT NULL,
    
    --  consider breaking into seperate table
    charges_information json NOT NULL DEFAULT '{}',

    end_to_end_reference text,

    fx json,

    numeric_reference text,
    reference text NOT NULL,
    payment_purpose text,
    payment_scheme text,

    scheme_payment_sub_type text,
    scheme_payment_type text,

    beneficiary_id  bigint REFERENCES parties (id),
    debator_id  bigint REFERENCES parties (id),
    sponsor_id  bigint REFERENCES parties (id),

    created_at timestamp NOT NULL DEFAULT current_timestamp,
    updated_at timestamp
);

-- parties
INSERT INTO "public"."parties"("id","account_number","bank_id","bank_id_code","account_type","account_name","name","address","account_number_code","created_at","updated_at")
VALUES
(1,E'23423423',E'adsf',E'sdf',E'1',E'sfs',E'asdf',E'sadf',E'asdf',E'2019-01-10 14:44:33.478303',NULL),
(2,E'324234',E'adsfdfs',E'dsaf',E'2',E'sadasda',E'asdas',E'fdsfs',E'sdfsdf',E'2019-01-10 14:44:33.478303',NULL),
(3,E'234234',E'sdfsdf',E'sdfs',E'2',E'werwe',E'sdfsdf',E'sdfsdf',E'dsfs',E'2019-01-10 14:44:33.478303',NULL);


-- payments
INSERT INTO "public"."payments"("id","amount","currency","charges_information","end_to_end_reference","fx","numeric_reference","reference","payment_purpose","payment_scheme","scheme_payment_sub_type","scheme_payment_type","beneficiary_id","debator_id","sponsor_id","created_at","updated_at")
VALUES
(1,E'13',E'GBP',E'{}',E'fg',NULL,E'234',E'fdsaf',E'sdf',E'sdf',E'sds',E'sdf',1,2,NULL,E'2019-01-10 14:45:28.57296',NULL);


-- resources
INSERT INTO "public"."resources"("id","path","method","created_at","updated_at")
VALUES
(1,E'/payment',E'POST',E'2019-01-09 00:59:01.938984',NULL),
(2,E'/payment',E'GET',E'2019-01-09 00:59:01.938984',NULL),
(3,E'/payment/{paymentID}',E'PUT',E'2019-01-10 15:03:35.979113',NULL),
(4,E'/payment/{paymentID}',E'DELETE',E'2019-01-10 15:03:35.979113',NULL),
(5,E'/resource',E'GET',E'2019-01-10 15:25:48.7435',NULL);

-- authorizations
INSERT INTO "public"."authorizations"("id","token","expires_at","created_at","updated_at")
VALUES
(1,E'sometoken',NULL,E'2019-01-09 00:59:12.610077',NULL);

-- resource authorizatinos
INSERT INTO "public"."resource_authorizations"("id","authorization_id","resource_id","created_at","updated_at")
VALUES
(1,1,1,E'2019-01-09 00:59:23.935678',NULL),
(2,1,2,E'2019-01-09 00:59:23.935678',NULL),
(3,1,3,E'2019-01-09 00:59:23.935678',NULL),
(4,1,4,E'2019-01-09 00:59:23.935678',NULL),
(5,1,5,E'2019-01-09 00:59:23.935678',NULL);

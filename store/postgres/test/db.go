package test

import (
	"testing"

	"gitlab.com/IyadAssaf/payment/store"
	"gitlab.com/IyadAssaf/payment/store/postgres"
)

// New - return a test postgres database connection
func New(t *testing.T) store.Storeable {
	// os.Setenv("DATABASE_URL", "postgres://127.0.0.1:5432/form3")
	p := postgres.New()
	if err := p.Connect(); err != nil {
		t.Fatal(err)
	}
	return p
}

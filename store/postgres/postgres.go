package postgres

import (
	"database/sql"
	"fmt"
	"os"

	"gitlab.com/IyadAssaf/payment/store/shared"

	// pq needs to be imported for database/sql to work with Postgres
	_ "github.com/lib/pq"
)

// Postgres - a postgres database connection
type Postgres struct {
	*sql.DB
	url string
}

// New returns a new postgres Store object
func New() *Postgres {
	return &Postgres{
		url: fmt.Sprintf("%s?sslmode=disable", os.Getenv("DATABASE_URL")),
	}
}

// Connect - connect to postgres and check that it's reachable
func (pg *Postgres) Connect() error {
	var err error
	if pg.DB, err = sql.Open("postgres", pg.url); err != nil {
		return err
	}
	pg.DB.SetMaxIdleConns(5)
	pg.DB.SetMaxOpenConns(10)

	return pg.Ping()
}

// Begin - overrides DB.Begin to return Txable instead of *sql.Tx
func (pg *Postgres) Begin() (shared.Txable, error) {
	return pg.DB.Begin()
}

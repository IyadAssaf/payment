package postgres

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConnect(t *testing.T) {
	os.Setenv("DATABASE_URL", "postgres://127.0.0.1:5432/form")
	p := New()

	if err := p.Connect(); err != nil {
		t.Fatal(err)
	}

	rows, err := p.Query("SELECT 1")
	if err != nil {
		t.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var n int
		if err = rows.Scan(&n); err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, 1, n)
	}
}

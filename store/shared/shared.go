package shared

import (
	"context"
	"database/sql"
)

// Txable - a transaction interface
type Txable interface {
	QueryContext(context.Context, string, ...interface{}) (*sql.Rows, error)
	Commit() error
	Rollback() error
}

// Rows - interface to *sql.Rows
type Rows interface {
	Close() error
	ColumnTypes() ([]*sql.ColumnType, error)
	Columns() ([]string, error)
	Err() error
	Next() bool
	NextResultSet() bool
	Scan(dest ...interface{}) error
}

FROM golang:1.11

RUN mkdir -p /go/src/gitlab.com/IyadAssaf/payment

WORKDIR /go/src/gitlab.com/IyadAssaf/payment

COPY . /go/src/gitlab.com/IyadAssaf/payment

RUN ./scripts/install_deps && go build ./cmd/api

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.4.0/wait /wait
RUN chmod +x /wait

ENV PORT 8080

CMD /wait && ./api

EXPOSE 8080
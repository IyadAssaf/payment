package common

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	validator "gopkg.in/go-playground/validator.v9"
)

var validate *validator.Validate

func init() {
	validate = validator.New()
}

// Request - a wrapper around http.Request with commonly used methods
type Request struct {
	*http.Request
}

// ParseBody - read the HTTP body and cast to the incoming object. The input param should be a pointer to an object
// Note that once this is called the .Body cannot be ready again
func (r *Request) ParseBody(params interface{}) error {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, &params)
}

// ParamsAreValid - return error if parameters are invalid
func (r *Request) ParamsAreValid(params interface{}) error {
	err := validate.Struct(params)
	if err != nil {
		errMsg := "Parameter(s) invalid or missing: "

		var errFields []string
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			return &ResponderBadRequest{"Bad request", 1}
		}

		for _, err := range errs {
			errFields = append(errFields, err.Field())
		}

		errMsg = fmt.Sprintf("%s%s", errMsg, strings.Join(errFields, ", "))
		return &ResponderBadRequest{errMsg, 1}
	}
	return nil
}

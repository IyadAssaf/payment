package common

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Responder - A wrapper around http.ResponseWriter to simplify common responses
type Responder struct {
	http.ResponseWriter
}

// standard error response JSON structure
type errorResponse struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

// Success - successful request response (status 200)
func (r *Responder) Success(resp interface{}) error {
	b, err := json.Marshal(resp)
	if err != nil {
		fmt.Println("Error marhsalling success resp", err)
		return r.Fail(err)
	}
	r.Header().Set("Content-Type", "application/json")
	r.WriteHeader(http.StatusOK)
	r.Write(b)
	return nil
}

// Fail - failed request response (status 500)
func (r *Responder) Fail(err error) error {
	r.Header().Set("Content-Type", "application/json")
	r.WriteHeader(http.StatusInternalServerError)
	r.Write([]byte(`{ "message": "Internal server error" }`))
	return err
}

// Unauthorized - request is unauthorized to complete action
func (r *Responder) Unauthorized() error {
	r.Header().Set("Content-Type", "application/json")
	r.WriteHeader(http.StatusUnauthorized)
	r.Write([]byte(`{ "message": "Unauthorized" }`))
	return nil
}

// ResponderBadRequest - bad request errors
type ResponderBadRequest struct {
	ErrorString string
	ErrorCode   int
}

// Error - return .ErrorString, satisfies error interface
func (rbr *ResponderBadRequest) Error() string {
	return rbr.ErrorString
}

// common bad request errors
var (
	ResponderBadRequestInvalidJSON = &ResponderBadRequest{"Requst malformed", 0}
	ResponderBadRequestToken       = &ResponderBadRequest{"Request token invalid or missing", 1}
	ResponderBadRequestParameter   = &ResponderBadRequest{"Parameter(s) invalid or missing", 1}
	ResponderBadRequestRejected    = &ResponderBadRequest{"Payment transaction rejected", 2}
)

// BadRequest - Response for when client has sent a bad request (status 400)
func (r *Responder) BadRequest(inputErr error) error {

	errObj := errorResponse{
		Message: "Bad request",
		Code:    0,
	}

	if badReqErr, ok := inputErr.(*ResponderBadRequest); ok {
		errObj = errorResponse{
			Message: badReqErr.ErrorString,
			Code:    badReqErr.ErrorCode,
		}
	}

	b, err := json.Marshal(errObj)
	if err != nil {
		return r.Fail(err)
	}
	r.Header().Set("Content-Type", "application/json")
	r.WriteHeader(http.StatusBadRequest)
	r.Write(b)
	return inputErr
}

// ResponderNotFound - Not found error definitions
type ResponderNotFound struct {
	ErrorString string
	ErrorCode   int
}

// Error - return .ErrorString, satisfies error interface
func (rnf *ResponderNotFound) Error() string {
	return rnf.ErrorString
}

var (
	// ResponderResourceNotFound - Generic not found error for if a resource has not been found
	ResponderResourceNotFound = &ResponderNotFound{"resource not found", 0}
)

// NotFound - Response for when resource is not found (status 404)
func (r *Responder) NotFound(inputErr error) error {

	errObj := errorResponse{
		Message: "resource not found",
		Code:    0,
	}
	if notFoundErr, ok := inputErr.(*ResponderNotFound); ok {
		errObj = errorResponse{
			Message: notFoundErr.ErrorString,
			Code:    notFoundErr.ErrorCode,
		}
	}

	b, err := json.Marshal(errObj)
	if err != nil {
		return r.Fail(err)
	}
	r.Header().Set("Content-Type", "application/json")
	r.WriteHeader(http.StatusNotFound)
	r.Write(b)
	return inputErr
}

// Conflict - Response for when unique constraint violated (status 409)
func (r *Responder) Conflict(inputErr error) error {
	errObj := errorResponse{
		Message: "conflict",
		Code:    0,
	}
	b, err := json.Marshal(errObj)
	if err != nil {
		return r.Fail(err)
	}
	r.Header().Set("Content-Type", "application/json")
	r.WriteHeader(http.StatusConflict)
	r.Write(b)
	return inputErr
}

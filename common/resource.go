package common

import (
	"context"
	"database/sql"

	"gitlab.com/IyadAssaf/payment/store"
	sharedStore "gitlab.com/IyadAssaf/payment/store/shared"
)

// Resource is an object to hold objects to pass along processes
type Resource struct {
	Store store.Storeable
}

// RequestContext is common information passed through requests
type RequestContext struct {
	Context      context.Context
	RequestToken *string
	Resource     *Resource

	// DB transaction for request
	Tx sharedStore.Txable
}

// Begin - start a store transaction
func (rc *RequestContext) Begin() error {
	var err error
	if rc.Tx, err = rc.Resource.Store.Begin(); err != nil {
		return err
	}
	return nil
}

// Commit - commit a transaction if running
func (rc *RequestContext) Commit() error {
	if rc.Tx == nil {
		return store.ErrTransactionNotStarted
	}
	return rc.Tx.Rollback()
}

// Rollback - rollback a transaction if running
func (rc *RequestContext) Rollback() error {
	if rc.Tx == nil {
		return store.ErrTransactionNotStarted
	}
	return rc.Tx.Rollback()
}

// Query - query the data store
func (rc *RequestContext) Query(q string, args ...interface{}) (*sql.Rows, error) {
	return rc.Resource.Store.QueryContext(rc.Context, q, args...)
}

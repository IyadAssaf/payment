# Payment api

### Quickstart, running server & database using docker
- `./scripts/run_tests_docker`
- Send requests to localhost:8080

### Run tests using docker:
- `./scripts/run_tests_docker`

### Example requests when running locally:
- Get resources `curl -H 'Authorization:sometoken' http://localhost:8080/resource`
- Get payments `curl -H 'Authorization:sometoken' http://localhost:8080/payment`

### Generate swagger documentation:
- Run `./spec/generate && cat spec.yaml`
- `spec.yaml` can be put into any swagger ui viewer, such as http://editor.swagger.io

### Notes:
- Testing is done using mocked database interface using github.com/DATA-DOG/go-sqlmock via my `./store/mock` package. I began to also include a test postgres connection using `./store/postgres/data/test`. This should be swappable with `mockStore.New()` in any of the `store.Storeable` test properties
package party

import (
	"database/sql"
	"fmt"

	"gitlab.com/IyadAssaf/payment/common"
)

// Type - type of party
type Type int

const (
	// TypeBeneficiary for when party is getting funds
	TypeBeneficiary = Type(iota)
	// TypeDebator for when party is having funds removed
	TypeDebator
	// TypeSponsor for when party is sponsoring funds transfer
	TypeSponsor
)

// Party of all parties
type Party struct {
	ID *int64 `json:"id"`

	AccountNumber     string  `json:"account_number"`
	BankID            string  `json:"bank_id"`
	BankIDCode        string  `json:"bank_id_code"`
	AccountType       int     `json:"account_type"`
	AccountName       *string `json:"account_name"`
	Name              *string `json:"name"`
	Address           *string `json:"address"`
	AccountNumberCode *string `json:"account_number_code"`
}

// ErrPartyNotFound - party could not be found
var ErrPartyNotFound = fmt.Errorf("Party does not exist")

// Load - load missing properties
func (p *Party) Load(ctx *common.RequestContext) error {
	var (
		err  error
		rows *sql.Rows
	)

	// find using either account_number or id
	if rows, err = ctx.Query(`
		SELECT id, account_number, bank_id, bank_id_code, account_type,
		account_name, name, address, account_number_code
		FROM parties
		WHERE id = $1
		LIMIT 1
	`, p.ID); err != nil {
		return err
	}
	defer rows.Close()

	rowCount := 0
	for rows.Next() {
		rowCount++
		if err = rows.Scan(
			&p.ID, &p.AccountNumber, &p.BankID, &p.BankIDCode, &p.AccountType,
			&p.AccountName, &p.Name, &p.Address, &p.AccountNumberCode,
		); err != nil {
			return err
		}
	}

	if rowCount == 0 {
		return ErrPartyNotFound
	}

	return nil
}

package party

import (
	"context"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/IyadAssaf/payment/common"
	mockStore "gitlab.com/IyadAssaf/payment/store/mock"
)

// RowsPartyLoadSuccess - mock data rows for loading a party
var RowsPartyLoadSuccess = func() sqlmock.Rows {
	return mockStore.MapToRows([]string{
		"id", "account_number", "bank_id", "bank_id_code", "account_type",
		"account_name", "name", "address", "account_number_code",
	}, []map[string]interface{}{
		{
			"id":                  1,
			"account_number":      "1234",
			"bank_id":             "2345",
			"bank_id_code":        "532",
			"account_type":        1,
			"account_name":        "iyad",
			"name":                "iyad",
			"address":             "iyad's house",
			"account_number_code": "1",
		},
	})
}

func TestLoad(t *testing.T) {
	tcs := []struct {
		name        string
		shouldError bool
		expectLen   int
		failQuery   bool
		returnRows  []sqlmock.Rows
	}{
		{
			name:        "success",
			shouldError: false,
			returnRows: []sqlmock.Rows{
				RowsPartyLoadSuccess(),
			},
		}, {
			name:        "fail scan",
			shouldError: true,
			failQuery:   false,
			returnRows: []sqlmock.Rows{
				mockStore.MapToRows([]string{"id", "account_number", "bank_id", "bank_id_code", "account_type",
					"account_name", "name", "address", "account_number_code"}, []map[string]interface{}{
					{
						"id":                  1,
						"account_number":      12345,
						"bank_id":             "2345",
						"bank_id_code":        32432,
						"account_type":        "23rf",
						"account_name":        "iyad",
						"name":                "iyad",
						"address":             "iyad's house",
						"account_number_code": "1",
					},
				}),
			},
		}, {
			name:        "fail query",
			shouldError: true,
			failQuery:   true,
			returnRows:  nil,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			ms := mockStore.New(tc.returnRows)
			ms.FailQuery = tc.failQuery

			ctx := &common.RequestContext{
				Resource: &common.Resource{
					Store: ms,
				},
				Context: context.Background(),
			}

			p := &Party{
				AccountNumber:     "1234",
				BankID:            "5678",
				BankIDCode:        "test",
				AccountType:       1,
				AccountName:       nil,
				Name:              nil,
				Address:           nil,
				AccountNumberCode: nil,
			}
			err := p.Load(ctx)
			if tc.shouldError {
				assert.NotNil(t, err)
				return
			}
			assert.Nil(t, err)
		})
	}
}

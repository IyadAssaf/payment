package payment

import (
	"database/sql"

	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/party"
)

// Find payments based on parameters
func Find(ctx *common.RequestContext, paymentID *int64, limit, offset int) ([]*Payment, error) {
	var (
		rows                               *sql.Rows
		err                                error
		ps                                 []*Payment
		benficiaryID, debatorID, sponsorID *int64
	)

	if rows, err = ctx.Query(`
		SELECT id, amount, currency, 
		end_to_end_reference, numeric_reference, reference, 
		payment_purpose, payment_scheme, scheme_payment_sub_type, 
		beneficiary_id, debator_id, sponsor_id, created_at::date
		FROM payments
		WHERE id = COALESCE($1, id)
		LIMIT $2
		OFFSET $3
	`, paymentID, limit, offset); err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		p := &Payment{Attributes: &Attributes{}}
		if err = rows.Scan(
			&p.ID, &p.Attributes.Amount, &p.Attributes.Currency,
			&p.Attributes.EndToEndReference, &p.Attributes.NumericReference, &p.Attributes.Reference,
			&p.Attributes.PaymentPurpose, &p.Attributes.PaymentScheme, &p.Attributes.SchemePaymentSubType,
			&benficiaryID, &debatorID, &sponsorID, &p.Attributes.ProcessingDate,
		); err != nil {
			return nil, err
		}

		if benficiaryID != nil {
			p.Attributes.Benficiary = &party.Party{ID: benficiaryID}
			if err = p.Attributes.Benficiary.Load(ctx); err != nil {
				return nil, err
			}
		}
		if debatorID != nil {
			p.Attributes.Debator = &party.Party{ID: debatorID}
			if err = p.Attributes.Debator.Load(ctx); err != nil {
				return nil, err
			}
		}
		if sponsorID != nil {
			p.Attributes.Sponsor = &party.Party{ID: sponsorID}
			if err = p.Attributes.Sponsor.Load(ctx); err != nil {
				return nil, err
			}
		}
		ps = append(ps, p)
	}

	return ps, nil
}

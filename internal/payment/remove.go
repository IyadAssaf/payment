package payment

import (
	"database/sql"

	"gitlab.com/IyadAssaf/payment/common"
)

// Remove payment by ID
func (p *Payment) Remove(ctx *common.RequestContext) error {
	var (
		rows *sql.Rows
		err  error
	)

	if rows, err = ctx.Query(`
		DELETE FROM payments 
		WHERE id = $1
		RETURNING id
	`, p.ID); err != nil {
		return err
	}
	defer rows.Close()

	rowCount := 0
	for rows.Next() {
		rowCount++
	}

	if rowCount == 0 {
		return ErrPaymentNotFound
	}

	return nil
}

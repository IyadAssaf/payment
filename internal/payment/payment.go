package payment

import (
	"fmt"
	"time"

	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/party"
)

// ErrPaymentNotFound - payment doesn't exist
var ErrPaymentNotFound = fmt.Errorf("Payment does not exist")

// Payment - an instance of a payment made
type Payment struct {
	ID         *int64      `json:"id"`
	Attributes *Attributes `json:"attributes"`
}

// Attributes to a payment
type Attributes struct {
	Amount             string              `json:"amount"`
	Benficiary         *party.Party        `json:"beneficiary_party"`
	Debator            *party.Party        `json:"debtor_party"`
	Sponsor            *party.Party        `json:"sponsorParty"`
	ChargesInformation *ChargesInformation `json:"charges_information"`
	Currency           string              `json:"currency"`
	EndToEndReference  string              `json:"end_to_end_reference"`
	FX                 *FX                 `json:"fx"`
	NumericReference   string              `json:"numeric_reference"`
	PaymentPurpose     string              `json:"payment_purpose"`
	PaymentScheme      string              `json:"payment_scheme"`
	PaymentType        string              `json:"payment_type"`
	ProcessingDate     *time.Time          `json:"processing_date"`
	Reference          string              `json:"reference"`

	SchemePaymentSubType string `json:"scheme_payment_sub_type"`
	SchemePaymentType    string `json:"scheme_payment_type"`
}

// ChargesInformation - breakdown information about the payment
type ChargesInformation struct {
	BearerCode              string          `json:"bearer_code"`
	SenderCharges           []*SenderCharge `json:"sender_charges"`
	RecieverChargesAmount   string          `json:"receiver_charges_amount"`
	RecieverChargesCurrency string          `json:"receiver_charges_currency"`
}

// SenderCharge - instance of a charge within the transaction
type SenderCharge struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
}

// FX - exchange rates for payment
type FX struct {
	ContractReference string `json:"contract_reference"`
	ExchangeRate      string `json:"exchange_rate"`
	OriginalAmount    string `json:"original_amount"`
	OriginalCurrency  string `json:"original_currency"`
}

// AddParty - loa party and add to payment
func (p *Payment) AddParty(ctx *common.RequestContext, tpe party.Type, ptyID *int64) error {
	pty := &party.Party{ID: ptyID}
	if err := pty.Load(ctx); err != nil {
		return err
	}

	switch tpe {
	case party.TypeBeneficiary:
		// do specific checks
		p.Attributes.Benficiary = pty
		break
	case party.TypeDebator:
		// do specific checks
		p.Attributes.Debator = pty
		break
	case party.TypeSponsor:
		// do specific checks
		p.Attributes.Sponsor = pty
		break
	}
	return nil
}

package payment

import (
	"database/sql"

	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/party"
)

// CreateParams - parameters to initiate a new payment
type CreateParams struct {
	Amount   string `json:"amount" validate:"required"`
	Currency string `json:"currency" validate:"required"`

	Reference         string `json:"reference" validate:"required"`
	EndToEndReference string `json:"end_to_end_reference" validate:"required"`
	NumericReference  string `json:"numeric_reference"  validate:"required"`

	PaymentPurpose string `json:"payment_purpose" validate:"required"`
	PaymentScheme  string `json:"payment_scheme" validate:"required"`
	PaymentType    string `json:"payment_type" validate:"required"`

	ChargesInformation   *ChargesInformation `json:"charges_information"`
	SchemePaymentSubType string              `json:"scheme_payment_sub_type"`
	SchemePaymentType    string              `json:"scheme_payment_type"`

	BenficiaryID *int64 `json:"beneficiary_id" validate:"required"`
	DebatorID    *int64 `json:"debtor_id" validate:"required"`
	SponsorID    *int64 `json:"sponsor_id"`
}

// Create - create a new Payment, insert into the store
func Create(ctx *common.RequestContext, params *CreateParams) (*Payment, error) {
	var (
		err error
		p   = &Payment{Attributes: &Attributes{
			Amount:   params.Amount,
			Currency: params.Currency,

			Reference:         params.Reference,
			EndToEndReference: params.EndToEndReference,
			NumericReference:  params.NumericReference,

			PaymentPurpose: params.PaymentPurpose,
			PaymentScheme:  params.PaymentScheme,
			PaymentType:    params.PaymentType,

			ChargesInformation:   params.ChargesInformation,
			SchemePaymentSubType: params.SchemePaymentSubType,
			SchemePaymentType:    params.SchemePaymentType,
		}}
	)

	if err = p.AddParty(ctx, party.TypeBeneficiary, params.BenficiaryID); err != nil {
		return nil, err
	}
	if err = p.AddParty(ctx, party.TypeDebator, params.DebatorID); err != nil {
		return nil, err
	}
	if params.SponsorID != nil {
		if err = p.AddParty(ctx, party.TypeSponsor, params.SponsorID); err != nil {
			return nil, err
		}
	}

	if err = p.Save(ctx); err != nil {
		return nil, err
	}
	return p, nil
}

// Save - insert or update payment in data storage
func (p *Payment) Save(ctx *common.RequestContext) error {
	var (
		rows                               *sql.Rows
		err                                error
		attr                               = p.Attributes
		benficiaryID, debatorID, sponsorID *int64
	)
	if attr.Debator != nil {
		debatorID = attr.Debator.ID
	}
	if attr.Benficiary != nil {
		benficiaryID = attr.Benficiary.ID
	}
	if attr.Sponsor != nil {
		sponsorID = attr.Sponsor.ID
	}

	// upsert query
	if rows, err = ctx.Query(`
		INSERT INTO payments (
			id, amount, currency, 
			end_to_end_reference, numeric_reference, reference, 
			payment_purpose, payment_scheme, scheme_payment_sub_type, 
			beneficiary_id, debator_id, sponsor_id
		)
		VALUES (
			COALESCE($1, nextval('payments_id_seq')), $2,  $3,  
			$4,  $5, $6, 
			$7, $8, $9, 
			$10, $11, $12
		)
		ON CONFLICT (id) DO UPDATE
		SET
			amount = $2,
			currency = $3, 
			end_to_end_reference = $4, 
			numeric_reference = $5,
			reference = $6, 
			payment_purpose = $7, 
			payment_scheme = $8, 
			scheme_payment_sub_type = $9, 
			beneficiary_id = $10, 
			debator_id = $11, 
			sponsor_id = $12,
			created_at = current_timestamp::date
		RETURNING id, created_at::date
	`, p.ID, attr.Amount, attr.Currency,
		attr.EndToEndReference, attr.NumericReference, attr.Reference,
		attr.PaymentPurpose, attr.PaymentScheme, attr.SchemePaymentSubType,
		benficiaryID, debatorID, sponsorID); err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		if err = rows.Scan(&p.ID, &p.Attributes.ProcessingDate); err != nil {
			return err
		}
	}
	return nil
}

package resource

import (
	"context"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/IyadAssaf/payment/common"
	mockStore "gitlab.com/IyadAssaf/payment/store/mock"
)

func TestFindForToken(t *testing.T) {
	tcs := []struct {
		name        string
		token       string
		shouldError bool
		expectLen   int
		failQuery   bool
		returnRows  []sqlmock.Rows
	}{
		{
			name:        "success",
			token:       "sometoken",
			shouldError: false,
			returnRows: []sqlmock.Rows{
				mockStore.MapToRows([]string{"path", "methods"}, []map[string]interface{}{
					{
						"path":    "/payment",
						"methods": "{POST,GET}",
					},
				}),
			},
			expectLen: 1,
		}, {
			name:        "success multiple",
			token:       "sometoken",
			shouldError: false,
			returnRows: []sqlmock.Rows{
				mockStore.MapToRows([]string{"path", "methods"}, []map[string]interface{}{
					{
						"path":    "/payment",
						"methods": "{POST,GET}",
					},
					{
						"path":    "/payment",
						"methods": "{DELETE}",
					},
				}),
			},
			expectLen: 2,
		}, {
			name:        "fail to parse methods",
			token:       "sometoken",
			shouldError: true,
			returnRows: []sqlmock.Rows{
				mockStore.MapToRows([]string{"path", "methods"}, []map[string]interface{}{
					{
						"path":    "/payment",
						"methods": `["POST","GET"]`,
					},
				}),
			},
		}, {
			name:        "fail to query",
			token:       "sometoken",
			shouldError: true,
			failQuery:   true,
			returnRows: []sqlmock.Rows{
				mockStore.MapToRows([]string{"path", "methods"}, []map[string]interface{}{
					{
						"path":    "/payment",
						"methods": `["POST","GET"]`,
					},
				}),
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			ms := mockStore.New(tc.returnRows)
			ms.FailQuery = tc.failQuery

			ctx := &common.RequestContext{
				Resource: &common.Resource{
					Store: ms,
				},
				Context: context.Background(),
			}
			rcs, err := FindPermissions(ctx, tc.token)
			if err != nil {
				if tc.shouldError {
					assert.NotNil(t, err)
				} else {
					t.Fatal(err)
				}
				return
			}

			assert.Equal(t, len(rcs), tc.expectLen)
		})
	}
}

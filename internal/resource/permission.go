package resource

import (
	"database/sql"
	"fmt"

	"github.com/lib/pq"

	"gitlab.com/IyadAssaf/payment/common"
)

// Resource - permission for resource
type Resource struct {
	Path    string   `json:"path"`
	Methods []string `json:"methods"`
}

// ErrTokenExpired - token has expired
var ErrTokenExpired = fmt.Errorf("token is expired")

// FindPermissions - find persmissions for token
func FindPermissions(ctx *common.RequestContext, token string) ([]*Resource, error) {
	var (
		rows *sql.Rows
		err  error
		rscs []*Resource
	)

	if rows, err = ctx.Query(`
		SELECT R.path, array_agg(R.method) AS "methods"
		FROM authorizations A
		JOIN resource_authorizations RA ON (A.id = RA.authorization_id)
		JOIN resources R ON (RA.resource_id = R.id)
		WHERE token = $1
		GROUP BY path
	`, token); err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		rsc := &Resource{}
		var methods pq.StringArray
		if err = rows.Scan(&rsc.Path, &methods); err != nil {
			return nil, err
		}
		rsc.Methods = []string(methods)
		rscs = append(rscs, rsc)
	}

	return rscs, nil
}

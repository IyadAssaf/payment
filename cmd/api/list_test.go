package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/store"
	mockStore "gitlab.com/IyadAssaf/payment/store/mock"
)

var returnPermissionRows = func() sqlmock.Rows {
	return mockStore.MapToRows([]string{
		"path", "methods",
	}, []map[string]interface{}{
		{
			"path":    "/payment",
			"methods": "{POST,GET}",
		}, {
			"path":    "/payment/{paymentID}",
			"methods": "{PUT,DELETE}",
		}, {
			"path":    "/resource",
			"methods": "{GET}",
		},
	})
}

var returnListPaymentRows = func() sqlmock.Rows {
	return mockStore.MapToRows([]string{
		"id", "amount", "currency",
		"end_to_end_reference", "numeric_reference", "reference",
		"payment_purpose", "payment_scheme", "scheme_payment_sub_type",
		"beneficiary_id", "debator_id", "sponsor_id", "created_at::date",
	}, []map[string]interface{}{
		{
			"id":                      1,
			"amount":                  "9.99",
			"currency":                "GBP",
			"end_to_end_reference":    "12345",
			"numeric_reference":       "56789",
			"reference":               "0987",
			"payment_purpose":         "64738",
			"payment_scheme":          "test",
			"scheme_payment_sub_type": "4378",
			"beneficiary_id":          1,
			"debator_id":              2,
			"sponsor_id":              3,
			"created_at::date":        time.Date(2019, 01, 01, 0, 0, 0, 0, time.UTC),
		},
	})
}

func TestListPayments(t *testing.T) {

	tcs := []struct {
		name                 string
		expectedStatus       int
		expectedPaymentCount int
		store                store.Storeable
	}{
		{
			name:                 "find none",
			expectedStatus:       http.StatusOK,
			expectedPaymentCount: 0,
			store: mockStore.New(
				[]sqlmock.Rows{
					returnPermissionRows(),
					mockStore.EmptyRow,
					mockStore.EmptyRow,
					mockStore.EmptyRow,
				},
			),
		}, {
			name:                 "find one",
			expectedStatus:       http.StatusOK,
			expectedPaymentCount: 1,
			store: mockStore.New(
				[]sqlmock.Rows{
					returnPermissionRows(),
					returnListPaymentRows(),
					retrunPartyRows(),
					retrunPartyRows(),
					retrunPartyRows(),
				},
			),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			r := NewRouter(&common.Resource{
				Store: tc.store,
			})
			srv := httptest.NewServer(r)
			defer srv.Close()

			resp, err := doRequest("GET", fmt.Sprintf("%s/payment", srv.URL), nil)
			if !assert.Nil(t, err) {
				t.Fatal(err)
			}

			if !assert.Equal(t, tc.expectedStatus, resp.StatusCode) {
				t.Fatal()
			}

			var output *listPaymentResponse
			err = resp.parseBody(&output)
			if !assert.Nil(t, err) {
				t.Fatal()
			}

			assert.Equal(t, tc.expectedPaymentCount, len(output.Data))
			if len(output.Data) != 0 {
				assert.NotNil(t, output.Data[0].Attributes.Benficiary)
				assert.NotNil(t, output.Data[0].Attributes.Debator)
				assert.NotNil(t, output.Data[0].Attributes.Sponsor)
			}
		})
	}
}

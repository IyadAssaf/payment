package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/resource"
	"gitlab.com/IyadAssaf/payment/store"
)

func main() {
	var (
		err error
		s   store.Storeable
	)
	if s, err = store.Init(); err != nil {
		log.Fatalf("Faied to connect to store %s", err.Error())
	}

	rsc := &common.Resource{Store: s}

	port := os.Getenv("PORT")
	if port == "" {
		port = "9000"
	}

	log.Fatal(http.ListenAndServe(
		fmt.Sprintf(":%s", port), NewRouter(rsc)),
	)
}

// NewRouter - create a new router
func NewRouter(rsc *common.Resource) *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Payment service"))
	}).Methods("GET")

	r.HandleFunc("/resource", Dispatch(rsc, GetResources)).Methods("GET")

	r.HandleFunc("/payment", Dispatch(rsc, ListPayments)).Methods("GET")
	r.HandleFunc("/payment", Dispatch(rsc, CreatePayment)).Methods("POST")
	r.HandleFunc("/payment/{paymentID}", Dispatch(rsc, UpdatePayment)).Methods("PUT")
	r.HandleFunc("/payment/{paymentID}", Dispatch(rsc, RemovePayment)).Methods("DELETE")

	return r
}

// Controller - Structure for all request handlers, contains context, responder and request objects
type Controller func(ctx *common.RequestContext, w common.Responder, r *common.Request) error

// Dispatch - Triggers the controller from the standard http.HandlerFunc method. Includes common functionality like getting and validating the "Request-Token" HTTP header and starting a context
func Dispatch(rsc *common.Resource, controller Controller) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		requestToken := r.Header.Get("Request-Token")
		ctx := &common.RequestContext{
			Context:      context.Background(),
			RequestToken: &requestToken,
			Resource:     rsc,
		}

		responder := common.Responder{ResponseWriter: w}
		authToken := r.Header.Get("Authorization")
		foundResources, err := resource.FindPermissions(ctx, authToken)
		if err != nil {
			fmt.Printf("Failed permissions %s", err.Error())
			responder.Fail(err)
			return
		}

		// change the r.URL.Path back from /payment/1 to /payment/{paymentID} for matching
		urlParams := mux.Vars(r)
		matchURL := r.URL.Path
		for k, v := range urlParams {
			matchURL = strings.Replace(matchURL, v, fmt.Sprintf("{%s}", k), -1)
		}

		var isAuthorized bool
	PATH:
		for _, fr := range foundResources {
			if fr.Path == matchURL {
				for _, mt := range fr.Methods {
					if mt == r.Method {
						isAuthorized = true
						break PATH
					}
				}
			}
		}

		if !isAuthorized {
			responder.Unauthorized()
			return
		}

		if err := controller(ctx, responder, &common.Request{Request: r}); err != nil {
			log.Println("Controller error", err)
		}
	}
}

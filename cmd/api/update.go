package main

import (
	"fmt"
	"strconv"

	"gitlab.com/IyadAssaf/payment/internal/party"

	"github.com/gorilla/mux"
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/payment"
)

// UpdatePayment - handler for updating payments
func UpdatePayment(ctx *common.RequestContext, w common.Responder, r *common.Request) error {
	var (
		err    error
		pID    *int64
		params *payment.CreateParams
		resp   = struct {
			Payment *payment.Payment `json:"payment"`
		}{}
	)

	{
		paymentID := mux.Vars(r.Request)["paymentID"]
		if paymentID == "" {
			return w.BadRequest(common.ResponderBadRequestParameter)
		}
		var parseErr error
		p, parseErr := strconv.Atoi(paymentID)
		if parseErr != nil {
			return w.BadRequest(common.ResponderBadRequestParameter)
		}
		p64 := int64(p)
		pID = &p64
	}

	{
		if err = r.ParseBody(&params); err != nil {
			return w.BadRequest(err)
		}
		if err = r.ParamsAreValid(params); err != nil {
			return w.BadRequest(err)
		}
	}

	var foundPayments []*payment.Payment
	if foundPayments, err = payment.Find(ctx, pID, 1, 0); err != nil {
		return w.Fail(err)
	}
	if len(foundPayments) == 0 {
		return w.NotFound(err)
	}

	resp.Payment = foundPayments[0]

	resp.Payment.Attributes.Amount = params.Amount
	resp.Payment.Attributes.Currency = params.Currency
	resp.Payment.Attributes.Reference = params.Reference
	resp.Payment.Attributes.EndToEndReference = params.EndToEndReference
	resp.Payment.Attributes.NumericReference = params.NumericReference
	resp.Payment.Attributes.PaymentPurpose = params.PaymentPurpose
	resp.Payment.Attributes.PaymentScheme = params.PaymentScheme
	resp.Payment.Attributes.PaymentType = params.PaymentType
	resp.Payment.Attributes.ChargesInformation = params.ChargesInformation
	resp.Payment.Attributes.SchemePaymentSubType = params.SchemePaymentSubType
	resp.Payment.Attributes.SchemePaymentType = params.SchemePaymentType

	// load parties, check if they exist
	{
		p := &party.Party{ID: params.DebatorID}
		if err = p.Load(ctx); err != nil {
			if err == party.ErrPartyNotFound {
				return w.NotFound(fmt.Errorf("Party %d not found", *params.DebatorID))
			}
			return w.Fail(err)
		}
		resp.Payment.Attributes.Debator = p
	}

	{
		p := &party.Party{ID: params.BenficiaryID}
		if err = p.Load(ctx); err != nil {
			if err == party.ErrPartyNotFound {
				return w.NotFound(fmt.Errorf("Party %d not found", *params.BenficiaryID))
			}
			return w.Fail(err)
		}
		resp.Payment.Attributes.Benficiary = p
	}

	if params.SponsorID != nil {
		p := &party.Party{ID: params.SponsorID}
		if err = p.Load(ctx); err != nil {
			if err == party.ErrPartyNotFound {
				return w.NotFound(fmt.Errorf("Party %d not found", *params.SponsorID))
			}
			return w.Fail(err)
		}
		resp.Payment.Attributes.Sponsor = p
	}

	if err = resp.Payment.Save(ctx); err != nil {
		return w.Fail(err)
	}

	return w.Success(resp)
}

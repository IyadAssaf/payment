package main

import (
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/payment"
)

// RemovePayment - handler for removing a payment
func RemovePayment(ctx *common.RequestContext, w common.Responder, r *common.Request) error {
	var (
		err error
		pID *int64
	)

	{
		paymentID := mux.Vars(r.Request)["paymentID"]
		if paymentID == "" {
			return w.BadRequest(common.ResponderBadRequestParameter)
		}
		var parseErr error
		p, parseErr := strconv.Atoi(paymentID)
		if parseErr != nil {
			return w.BadRequest(common.ResponderBadRequestParameter)
		}
		p64 := int64(p)
		pID = &p64
	}

	var foundPayments []*payment.Payment
	if foundPayments, err = payment.Find(ctx, pID, 1, 0); err != nil {
		return w.Fail(err)
	}

	if len(foundPayments) == 0 {
		return w.NotFound(err)
	}

	resp := struct {
		Payment *payment.Payment `json:"payment"`
	}{
		Payment: foundPayments[0],
	}
	if err = resp.Payment.Remove(ctx); err != nil {
		return w.Fail(err)
	}

	return w.Success(resp)
}

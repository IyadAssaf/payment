package main

import (
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/resource"
)

// GetResources - get resources the client is able to access
func GetResources(ctx *common.RequestContext, w common.Responder, r *common.Request) error {
	var (
		err  error
		resp struct {
			Resources []*resource.Resource `json:"resources"`
		}
	)

	if resp.Resources, err = resource.FindPermissions(ctx, r.Header.Get("Authorization")); err != nil {
		return w.Fail(err)
	}
	return w.Success(resp)
}

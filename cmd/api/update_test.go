package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/IyadAssaf/payment/internal/payment"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/store"
	mockStore "gitlab.com/IyadAssaf/payment/store/mock"
	"gitlab.com/IyadAssaf/payment/testdata"
)

func TestUpdatePayment(t *testing.T) {

	tcs := []struct {
		name           string
		expectedStatus int
		store          store.Storeable
		params         *payment.CreateParams
	}{
		{
			name:           "success",
			expectedStatus: http.StatusOK,
			store: mockStore.New(
				[]sqlmock.Rows{
					returnPermissionRows(),
					returnListPaymentRows(),
					retrunPartyRows(),
					retrunPartyRows(),
					retrunPartyRows(),

					// load parties sent up
					retrunPartyRows(),
					retrunPartyRows(),
					retrunPartyRows(),

					retrunUpsertPaymentRows(),
				},
			),
			params: &testdata.CreatePaymentParamsValid,
		}, {
			name:           "not found",
			expectedStatus: http.StatusNotFound,
			store: mockStore.New(
				[]sqlmock.Rows{
					returnPermissionRows(),
					mockStore.EmptyRow,
				},
			),
			params: &testdata.CreatePaymentParamsValid,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			r := NewRouter(&common.Resource{
				Store: tc.store,
			})
			srv := httptest.NewServer(r)
			defer srv.Close()

			resp, err := doRequest("PUT", fmt.Sprintf("%s/payment/%d", srv.URL, 1), tc.params)
			if !assert.Nil(t, err) {
				t.Fatal()
			}

			if !assert.Equal(t, tc.expectedStatus, resp.StatusCode) {
				t.Fatal()
			}
		})
	}
}

package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/store"
	mockStore "gitlab.com/IyadAssaf/payment/store/mock"
)

func TestRemovePayment(t *testing.T) {

	tcs := []struct {
		name           string
		expectedStatus int
		store          store.Storeable
	}{
		{
			name:           "success",
			expectedStatus: http.StatusOK,
			store: mockStore.New(
				[]sqlmock.Rows{
					returnPermissionRows(),
					returnListPaymentRows(),
					retrunPartyRows(),
					retrunPartyRows(),
					retrunPartyRows(),
					mockStore.MapToRows([]string{"id"}, []map[string]interface{}{
						{
							"id": 1,
						},
					}),
				},
			),
		}, {
			name:           "not found",
			expectedStatus: http.StatusNotFound,
			store: mockStore.New(
				[]sqlmock.Rows{
					returnPermissionRows(),
					mockStore.EmptyRow,
				},
			),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			r := NewRouter(&common.Resource{
				Store: tc.store,
			})
			srv := httptest.NewServer(r)
			defer srv.Close()

			resp, err := doRequest("DELETE", fmt.Sprintf("%s/payment/%d", srv.URL, 1), nil)
			if !assert.Nil(t, err) {
				t.Fatal()
			}

			if !assert.Equal(t, tc.expectedStatus, resp.StatusCode) {
				t.Fatal()
			}
		})
	}
}

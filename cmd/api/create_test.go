package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/payment"
	mockStore "gitlab.com/IyadAssaf/payment/store/mock"
	"gitlab.com/IyadAssaf/payment/testdata"
)

var retrunPartyRows = func() sqlmock.Rows {
	return mockStore.MapToRows([]string{"id", "account_number", "bank_id", "bank_id_code", "account_type",
		"account_name", "name", "address", "account_number_code"}, []map[string]interface{}{
		{
			"id":                  1,
			"account_number":      1234,
			"bank_id":             "2345",
			"bank_id_code":        32432,
			"account_type":        1,
			"account_name":        "iyad",
			"name":                "iyad",
			"address":             "iyad's house",
			"account_number_code": "1",
		},
	})
}

var retrunUpsertPaymentRows = func() sqlmock.Rows {
	return mockStore.MapToRows([]string{"id", "created_at::date"}, []map[string]interface{}{
		{
			"id":               1,
			"created_at::date": time.Now(),
		},
	})
}

func TestCreatePayment(t *testing.T) {

	tcs := []struct {
		name           string
		params         *payment.CreateParams
		expectedStatus int
	}{
		{
			name: "missing parties",
			params: &payment.CreateParams{
				BenficiaryID: testdata.Party.ID,
			},
			expectedStatus: http.StatusBadRequest,
		}, {
			name:           "empty body",
			params:         nil,
			expectedStatus: http.StatusBadRequest,
		}, {
			name:           "create payment",
			params:         &testdata.CreatePaymentParamsValid,
			expectedStatus: http.StatusOK,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			r := NewRouter(&common.Resource{
				Store: mockStore.New(
					[]sqlmock.Rows{
						returnPermissionRows(),
						retrunPartyRows(),
						retrunPartyRows(),
						retrunPartyRows(),
						retrunUpsertPaymentRows(),
					},
				),
			})
			srv := httptest.NewServer(r)
			defer srv.Close()

			resp, err := doRequest("POST", fmt.Sprintf("%s/payment", srv.URL), tc.params)
			if !assert.Nil(t, err) {
				t.Fatal()
			}

			if !assert.Equal(t, tc.expectedStatus, resp.StatusCode) {
				t.Fatal()
			}

			var output *createPaymentResponse
			err = resp.parseBody(&output)
			if !assert.Nil(t, err) {
				t.Fatal()
			}

			// if all things go well
			if resp.StatusCode == http.StatusOK {
				assert.Equal(t, *tc.params.BenficiaryID, *output.Payment.Attributes.Benficiary.ID)
			}
		})
	}
}

type testResponse struct {
	*http.Response
}

func (tr *testResponse) parseBody(params interface{}) error {
	b, err := ioutil.ReadAll(tr.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(b, &params)
}

func doRequest(method, url string, jsonBody interface{}) (*testResponse, error) {
	var (
		err    error
		client = &http.Client{}
		req    *http.Request
		body   io.Reader
		tr     = &testResponse{}
	)

	if jsonBody != nil {
		var b []byte
		if b, err = json.Marshal(jsonBody); err != nil {
			return nil, err
		}
		body = bytes.NewBuffer(b)
	}

	if req, err = http.NewRequest(method, url, body); err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "sometoken")

	if tr.Response, err = client.Do(req); err != nil {
		return nil, err
	}
	return tr, nil
}

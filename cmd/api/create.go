package main

import (
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/party"
	"gitlab.com/IyadAssaf/payment/internal/payment"
)

type createPaymentResponse struct {
	Payment *payment.Payment `json:"payment"`
}

// CreatePayment - handler for creating payments
func CreatePayment(ctx *common.RequestContext, w common.Responder, r *common.Request) error {
	var (
		err    error
		params *payment.CreateParams
		resp   = &createPaymentResponse{}
	)

	{
		if err = r.ParseBody(&params); err != nil {
			return w.BadRequest(err)
		}
		if err = r.ParamsAreValid(params); err != nil {
			return w.BadRequest(err)
		}
	}

	if resp.Payment, err = payment.Create(ctx, params); err != nil {
		if err == party.ErrPartyNotFound {
			return w.NotFound(err)
		}
		return w.Fail(err)
	}

	return w.Success(resp)
}

package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/IyadAssaf/payment/common"
	mockStore "gitlab.com/IyadAssaf/payment/store/mock"
)

func TestGetResources(t *testing.T) {
	tcs := []struct {
		name           string
		token          string
		returnRows     []sqlmock.Rows
		expectedStatus int
	}{
		{
			name:  "success",
			token: "sometoken",
			returnRows: []sqlmock.Rows{
				returnPermissionRows(),
				mockStore.MapToRows([]string{"path", "methods"}, []map[string]interface{}{
					{
						"path":    "/payment",
						"methods": "{POST,GET}",
					},
				}),
			},
			expectedStatus: http.StatusOK,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			r := NewRouter(&common.Resource{
				Store: mockStore.New(tc.returnRows),
			})
			srv := httptest.NewServer(r)
			defer srv.Close()

			resp, err := doRequest("GET", fmt.Sprintf("%s/resource", srv.URL), nil)
			if !assert.Nil(t, err) {
				t.Fatal()
			}

			if !assert.Equal(t, tc.expectedStatus, resp.StatusCode) {
				t.Fatal()
			}
		})
	}
}

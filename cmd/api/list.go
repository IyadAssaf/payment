package main

import (
	"gitlab.com/IyadAssaf/payment/common"
	"gitlab.com/IyadAssaf/payment/internal/payment"
)

type listPaymentResponse struct {
	Data []*payment.Payment `json:"data"`
}

// ListPayments - handler for listing payments
func ListPayments(ctx *common.RequestContext, w common.Responder, r *common.Request) error {
	var (
		resp   = &listPaymentResponse{}
		err    error
		limit  = 10
		offset = 0
	)

	if resp.Data, err = payment.Find(ctx, nil, limit, offset); err != nil {
		return w.Fail(err)
	}

	return w.Success(resp)
}

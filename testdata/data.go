package testdata

import (
	"gitlab.com/IyadAssaf/payment/internal/party"
	"gitlab.com/IyadAssaf/payment/internal/payment"
)

// Party - test party object
var Party = party.Party{
	ID: func() *int64 {
		i := int64(1)
		return &i
	}(),
	AccountNumber:     "1234",
	BankID:            "5678",
	BankIDCode:        "test",
	AccountType:       1,
	AccountName:       nil,
	Name:              nil,
	Address:           nil,
	AccountNumberCode: nil,
}

// CreatePaymentParamsValid - valid create payment request params
var CreatePaymentParamsValid = payment.CreateParams{
	Amount:   "1234",
	Currency: "GBP",

	Reference:         "test",
	EndToEndReference: "test",
	NumericReference:  "test",

	PaymentPurpose: "test",
	PaymentScheme:  "test",
	PaymentType:    "test",

	ChargesInformation:   nil,
	SchemePaymentSubType: "test",
	SchemePaymentType:    "test",

	BenficiaryID: Party.ID,
	DebatorID:    Party.ID,
	SponsorID:    Party.ID,
}
